<?php

namespace Drupal\webform_mapper\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\webform\Annotation\WebformHandler;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\Utility\WebformElementHelper;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform_mapper\Utility\WebformMapperYaml;
use Drupal\webform_mapper\Mapping\Definition;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\ExpressionLanguage\SyntaxError;

/**
 * Webform submission debug handler.
 *
 * @WebformHandler(
 *   id = "webform_mapper",
 *   label = @Translation("Webform Mapper"),
 *   category = @Translation("Development"),
 *   description = @Translation("Map webform data to and from entities."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_REQUIRED,
 * )
 */
class WebformMapperHandler extends WebformHandlerBase {

  /**
   * @var \Drupal\webform_mapper\Utility\NavigatorFactory
   */
  protected $navigatorFactory;

  /**
   * @var \Drupal\webform_mapper\Utility\LogHelper
   */
  protected $logHelper;

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->navigatorFactory = $container->get('webform_mapper.navigator_factory');
    $instance->logHelper = $container->get('webform_mapper.log_helper');
    return $instance;
  }

  public function defaultConfiguration() {
    return [
      'dry_run' => FALSE,
      'debug' => FALSE,
      'mapping_definition' =>[],
    ];
  }

  /**
   * @inheritDoc
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['dry_run'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Dry run'),
      '#default_value' => $this->configuration['dry_run'],
    ];
    $form['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug'),
      '#default_value' => $this->configuration['debug'],
    ];
    // @todo Add codemirror.
    $form['mapping_definition'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Mapping definition'),
      '#rows' => '20',
      '#default_value' => $this->configuration['mapping_definition'],
      '#description' => $this->t('Add Yaml "mappings" key with mappings like "path.to.target: <a href="@link">expression</a>". Use "entity" function to create an entity from values. You have to save entities yourself.'
        , ['@link' => 'https://symfony.com/doc/2.7/components/expression_language/syntax.html']),
    ];
    return $form;
  }

  /**
   * @inheritDoc
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $yml = $form_state->getValue('mapping_definition');
    try {
      $mapperDefinition = Definition::fromYaml($yml);
      // @todo Do a stub mapping to validate.
    } catch (SyntaxError $e) {
      $form_state->setErrorByName('mapping_definition', $e->getMessage());
    }
  }

  /**
   * @inheritDoc
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['dry_run'] = (bool) $form_state->getValue('dry_run');
    $this->configuration['debug'] = (bool) $form_state->getValue('debug');
    $this->configuration['mapping_definition'] = $form_state->getValue('mapping_definition');
  }

  public function postCreate(WebformSubmissionInterface $webformSubmission) {
    $this->processSubmission($webformSubmission, 'postcreate');
  }

  public function postLoad(WebformSubmissionInterface $webformSubmission) {
    $this->processSubmission($webformSubmission, 'postload');
  }

  public function preSave(WebformSubmissionInterface $webformSubmission) {
    $this->processSubmission($webformSubmission, 'presave');
  }

  public function postSave(WebformSubmissionInterface $webformSubmission, $update = TRUE) {
    $this->processSubmission($webformSubmission, 'postsave');
  }

  public function prepareForm(WebformSubmissionInterface $webformSubmission, $operation, FormStateInterface $form_state) {
    $this->processSubmission($webformSubmission, "prepareform-$operation");
  }

  private function processSubmission(WebformSubmissionInterface $webformSubmission, string $trigger): void {
    $dry_run = (bool) $this->configuration['dry_run'];
    $debug = (bool) $this->configuration['debug'];
    $navigator = $this->navigatorFactory->makeNavigator($trigger, $webformSubmission, $dry_run, $debug);
    $yml = $this->configuration['mapping_definition'];
    $mappingDefinition = Definition::fromYaml($yml);
    try {
      $mappingDefinition->mappings->map($navigator);
      if ($navigator->has('data_changes')) {
        $webformChanges = $navigator->getByPath('data_changes')->unwrap();
        if ($webformChanges) {
          $webformSubmission->setData($webformChanges + $webformSubmission->getData());
        }
      }
      if ($debug) {
        $this->logHelper->dumpData($navigator->unwrap(), "WebformMapper Debug: {$webformSubmission->getWebform()->id()}|$trigger|{$webformSubmission->serial()}");
      }
    } catch (\Throwable $e) {
      $this->logHelper->logError($e, $debug);
    }
  }

}
