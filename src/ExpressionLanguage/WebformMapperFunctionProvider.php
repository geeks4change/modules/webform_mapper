<?php

namespace Drupal\webform_mapper\ExpressionLanguage;

use Drupal\webform\WebformInterface;
use Drupal\webform_mapper\Navigator\ArrayReadOnlyNavigator;
use Drupal\webform_mapper\Utility\AutoMapper;
use Drupal\webform_mapper\Utility\EntityHelper;
use Symfony\Component\ExpressionLanguage\ExpressionFunction;
use Symfony\Component\ExpressionLanguage\ExpressionFunctionProviderInterface;

class WebformMapperFunctionProvider implements ExpressionFunctionProviderInterface {

  /**
   * @var \Drupal\webform_mapper\Utility\EntityHelper
   */
  private $entityHelper;

  /**
   * @var AutoMapper
   */
  private $autoMapper;

  /**
   * WebformMapperFunctionProvider constructor.
   * @param \Drupal\webform_mapper\Utility\EntityHelper $entityHelper
   * @param \Drupal\webform_mapper\Utility\AutoMapper $autoMapper
   */
  public function __construct(EntityHelper $entityHelper, AutoMapper $autoMapper) {
    $this->entityHelper = $entityHelper;
    $this->autoMapper = $autoMapper;
  }

  public function getFunctions() {
    $entityHelper = EntityHelper::class;
    $nav = ArrayReadOnlyNavigator::class;
    $autoMapper = AutoMapper::class;
    return [
      new ExpressionFunction(
        'entity_create',
        function (string $entityTypeExpression, string $valueExpression, $save) use ($entityHelper, $nav) {
          return "$entityHelper::create()->entityCreate($nav::unwrapIfNeeded($entityTypeExpression), $nav::unwrapIfNeeded({$valueExpression}, $save && empty($nav::unwrap(\$dry_run)))";
        },
        function ($vars, $entityType, $values, $save) {
          return $this->entityHelper->entityCreate(
            ArrayReadOnlyNavigator::unwrapIfNeeded($entityType),
            ArrayReadOnlyNavigator::unwrapIfNeeded($values),
            ArrayReadOnlyNavigator::unwrapIfNeeded($save)
              && empty(ArrayReadOnlyNavigator::unwrapIfNeeded($vars['dry_run'])),
            ArrayReadOnlyNavigator::unwrapIfNeeded($vars['debug'])
          );
        }
      ),
      new ExpressionFunction(
        'entity_load',
        function (string $entityTypeExpression, string $idExpression) use ($entityHelper, $nav) {
          return "$entityHelper::create()->entityLoad($nav::unwrapIfNeeded($entityTypeExpression), $nav::unwrapIfNeeded($idExpression))";
        },
        function ($vars, $entityType, $id) {
          return $this->entityHelper->entityLoad(
            ArrayReadOnlyNavigator::unwrapIfNeeded($entityType),
            ArrayReadOnlyNavigator::unwrapIfNeeded($id)
          );
        }
      ),
      new ExpressionFunction(
        'config_create',
        function (string $configNameExpression, string $valueExpression, $save) use ($entityHelper, $nav) {
          return "$entityHelper::create()->entityCreate($nav::unwrapIfNeeded($configNameExpression), $nav::unwrapIfNeeded({$valueExpression}, $save && empty($nav::unwrap(\$dry_run)))";
        },
        function ($vars, $configName, $values, $save) {
          return $this->entityHelper->configCreate(
            ArrayReadOnlyNavigator::unwrapIfNeeded($configName),
            ArrayReadOnlyNavigator::unwrapIfNeeded($values),
            ArrayReadOnlyNavigator::unwrapIfNeeded($save)
              && empty(ArrayReadOnlyNavigator::unwrapIfNeeded($vars['dry_run'])),
            ArrayReadOnlyNavigator::unwrapIfNeeded($vars['debug'])
          );
        }
      ),
      new ExpressionFunction(
        'config_load',
        function (string $configNameExpression) use ($entityHelper, $nav) {
          return "$entityHelper::create()->configLoad($nav::unwrapIfNeeded($configNameExpression))";
        },
        function ($vars, $configName) {
          return $this->entityHelper->configLoad(
            ArrayReadOnlyNavigator::unwrapIfNeeded($configName)
          );
        }
      ),
      // We need to pass the webform id explicitly here and can not inject it, so
      // to leave open the door for compiling.
      new ExpressionFunction(
        'automapin',
        function ($webformExpression, $webformSubmissionDataExpression) use ($autoMapper, $nav) {
          return "$autoMapper::create()->automapin($nav::unwrapIfNeeded($webformExpression), $nav::unwrapIfNeeded($webformSubmissionDataExpression))";
        },
        function ($vars, $webformId, $webformSubmissionData) {
          return $this->autoMapper->automapin(
            ArrayReadOnlyNavigator::unwrapIfNeeded($webformId),
            ArrayReadOnlyNavigator::unwrapIfNeeded($webformSubmissionData)
          );
        }
      ),
      new ExpressionFunction(
        'automapout',
        function ($webformExpression, $elementDataExpression) use ($autoMapper, $nav) {
          return "$autoMapper::create()->automapout($nav::unwrapIfNeeded($webformExpression), $nav::unwrapIfNeeded($elementDataExpression))";
        },
        function ($vars, $webformId, $elementData) {
          return $this->autoMapper->automapout(
            ArrayReadOnlyNavigator::unwrapIfNeeded($webformId),
            ArrayReadOnlyNavigator::unwrapIfNeeded($elementData)
          );
        }
      ),
    ];
  }

}
