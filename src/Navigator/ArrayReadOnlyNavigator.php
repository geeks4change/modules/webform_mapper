<?php

namespace Drupal\webform_mapper\Navigator;

use Drupal\webform_mapper\Utility\WebformMapperNestedArray;

class ArrayReadOnlyNavigator implements ReadOnlyNavigatorInterface {

  /**
   * @var mixed
   */
  protected $data;

  protected function __construct($data) {
    $this->data = $data;
  }

  public static function unwrapIfNeeded($data) {
    if ($data instanceof ReadOnlyNavigatorInterface) {
      return $data->unwrap();
    }
    else {
      return $data;
    }
  }

  public static function create($data) {
    return new static($data);
  }

  public function unwrap() {
    return $this->data;
  }

  public function __get(string $name): ReadOnlyNavigatorInterface {
    return $this->get($name);
  }

  public function get(string $name): ReadOnlyNavigatorInterface {
    // We need this as symfony expressions cough on foo.0
    return static::create($this->data[$name] ?? NULL);
  }

  public function getByPath(string $path): ReadOnlyNavigatorInterface {
    return ArrayReadOnlyNavigator::create(WebformMapperNestedArray::get($this->data, $path));
  }

  public function __isset(string $name): bool {
    return $this->has($name);
  }


  public function has(string $name): bool {
    return isset($this->data[$name]);
  }

}
