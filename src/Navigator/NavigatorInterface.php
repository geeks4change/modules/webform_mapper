<?php

namespace Drupal\webform_mapper\Navigator;

/**
 * Interface AccessorInterface
 */
interface NavigatorInterface extends ReadOnlyNavigatorInterface {

  public function setDataByPath(string $path, $data): void;

}
