<?php

namespace Drupal\webform_mapper\Navigator;

class NavigatorReadOnlyWrapper implements ReadOnlyNavigatorInterface {

  /**
   * @var \Drupal\webform_mapper\Navigator\ReadOnlyNavigatorInterface
   */
  protected $navigator;

  public function __construct(ReadOnlyNavigatorInterface $navigator) {
    $this->navigator = $navigator;
  }

  public static function create(ReadOnlyNavigatorInterface $decorated) {
    return new static($decorated);
  }

  public function unwrap() {
    return $this->navigator->unwrap();
  }

  public function __get(string $name): ReadOnlyNavigatorInterface {
    return $this->navigator->__get($name);
  }

  public function get(string $name): ReadOnlyNavigatorInterface {
    return $this->navigator->get($name);
  }

  public function __isset(string $name): bool {
    return $this->navigator->__isset($name);
  }

  public function getByPath(string $path): ReadOnlyNavigatorInterface {
    return $this->navigator->getByPath($path);
  }

  public function has(string $name): bool {
    return $this->navigator->has($name);
  }

}
