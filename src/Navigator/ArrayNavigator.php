<?php

namespace Drupal\webform_mapper\Navigator;

use Drupal\webform_mapper\Utility\WebformMapperNestedArray;

class ArrayNavigator extends ArrayReadOnlyNavigator implements NavigatorInterface {

  public function setDataByPath(string $path, $data): void {
    $ref =& WebformMapperNestedArray::get($this->data, $path);
    $ref = $data;
  }

}
