<?php

namespace Drupal\webform_mapper\Navigator;

class NavigatorNavigator implements NavigatorInterface {

  /**
   * @var \Drupal\webform_mapper\Navigator\ReadOnlyNavigatorInterface[]
   */
  protected $navigators = [];

  /**
   * NavigatorNavigator constructor.
   * @param \Drupal\webform_mapper\Navigator\ReadOnlyNavigatorInterface[] $navigators
   */
  private function __construct(array $navigators) {
    $this->navigators = $navigators;
  }

  /**
   * NavigatorNavigator constructor.
   * @param \Drupal\webform_mapper\Navigator\ReadOnlyNavigatorInterface[] $navigators
   */
  public static function create(array $navigators) {
    foreach ($navigators as $key => $navigator) {
      if (!$navigator instanceof ReadOnlyNavigatorInterface) {
        throw new \UnexpectedValueException("Invalid value ($key): " . var_export($navigator, TRUE));
      }
    }
    return new static($navigators);
  }

  /**
   * @return \Drupal\webform_mapper\Navigator\ReadOnlyNavigatorInterface[]
   */
  public function getNavigators(): array {
    return $this->navigators;
  }

  /**
   * @return \Drupal\webform_mapper\Navigator\ReadOnlyNavigatorInterface[]
   */
  public function getReadOnlyNavigators(): array {
    return array_map(function (NavigatorInterface $navigator) {
      return ($navigator instanceof ReadOnlyNavigatorInterface)
        ? $navigator : NavigatorReadOnlyWrapper::create($navigator);
    }, $this->navigators);
  }

  public function __get(string $name): ReadOnlyNavigatorInterface {
    return $this->get($name);
  }

  public function get(string $name): ReadOnlyNavigatorInterface {
    return $this->navigators[$name] ?? $this->throwInvalidKey($name);
  }

  public function __isset(string $name): bool {
    return isset($this->navigators[$name]);
  }

  public function getByPath(string $path): ReadOnlyNavigatorInterface {
    if (!$path) {
      return $this;
    }
    [$first, $rest] = explode('.', $path, 2) + [1 => ''];
    $navigator = $this->__get($first);
    return $navigator->getByPath($rest);
  }

  public function unwrap() {
    $value = [];
    foreach ($this->navigators as $name => $navigator) {
      $value[$name] = $navigator->unwrap();
    }
    return $value;
  }

  private function throwInvalidKey(string $name): ReadOnlyNavigatorInterface {
    throw new \RuntimeException("Invalid key: $name");
  }

  public function setDataByPath(string $path, $data): void {
    if (!$path) {
      throw new \RuntimeException("Can not set this.");
    }
    [$first, $rest] = explode('.', $path, 2) + [1 => ''];
    $navigator = $this->__get($first);
    $navigator->setDataByPath($rest, $data);
  }

  public function has(string $name): bool {
    return isset($this->navigators[$name]);
  }

}
