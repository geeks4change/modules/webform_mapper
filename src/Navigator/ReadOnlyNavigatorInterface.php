<?php

namespace Drupal\webform_mapper\Navigator;

interface ReadOnlyNavigatorInterface {

  public function __get(string $name): ReadOnlyNavigatorInterface;

  public function get(string $name): ReadOnlyNavigatorInterface;

  public function __isset(string $name): bool;

  public function getByPath(string $path): ReadOnlyNavigatorInterface;

  public function has(string $name): bool;

    /**
   * @return mixed
   */
  public function unwrap();

}
