<?php

namespace Drupal\webform_mapper\Utility;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldItemListInterface;

class EntityHelper {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Drupal\webform_mapper\Utility\LogHelper
   */
  protected $logHelper;

  /**
   * EntityHelper constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   * @param \Drupal\webform_mapper\Utility\LogHelper $logHelper
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, ConfigFactoryInterface $configFactory, LogHelper $logHelper) {
    $this->entityTypeManager = $entityTypeManager;
    $this->configFactory = $configFactory;
    $this->logHelper = $logHelper;
  }

  public function entityCreate(string $entityTypeId, array $values, bool $save, bool $debug): ?EntityInterface {
    $idKey = $this->entityTypeManager->getDefinition($entityTypeId)->getKey('id');
    $entityStorage = $this->entityTypeManager->getStorage($entityTypeId);
    $id = $this->extractId($values[$idKey] ?? NULL);
    if (isset($id) && ($entity = $entityStorage->load($id))) {
      if ($entity instanceof ContentEntityInterface) {
        foreach ($values as $key => $value) {
          $entity->get($key)->setValue($value);
        }
      }
      elseif ($entity instanceof ConfigEntityInterface) {
        $data = $entity->toArray();
        foreach (NestedArray::mergeDeep($values, $data) as $key => $value) {
          $entity->set($key, $value);
        }
      }
      else {
        throw new \UnexpectedValueException("Can not handle entity of type $entityTypeId");
      }
    }
    else {
      $entity = $entityStorage->create($values);
    }
    // Pipe all content entity fields through ::processDefaultValue(),
    // we need this e.g. to remove the timezone suffix of date fields.
    if ($entity instanceof ContentEntityInterface) {
      foreach ($entity as $fieldItemList) {
        assert($fieldItemList instanceof FieldItemListInterface);
        $fieldItemList->setValue(
          $fieldItemList::processDefaultValue(
            $fieldItemList->getValue(), $entity, $fieldItemList->getFieldDefinition()
          )
        );
      }
    }
    if ($save) {
      try {
        $entity->save();
      } catch (\Exception $e) {
        $this->logHelper->logError($e, $debug);
      }
    }
    return $entity;
  }

  public function entityLoad(string $entityTypeId, $id): ?EntityInterface {
    if (!is_string($id) && !is_int($id)) {
      $type = gettype($id);
      throw new \RuntimeException("entity_load id must be of type int or string, given: $type");
    }
    $entityStorage = $this->entityTypeManager->getStorage($entityTypeId);
    $entity = $entityStorage->load($id);
    return $entity;
  }

  public function configCreate(string $configName, array $values, bool $save, bool $debug): Config {
    $config = $this->configFactory->getEditable($configName);
    $data = $config->get();
    foreach (NestedArray::mergeDeep($values, $data) as $key => $value) {
      $config->set($key, $value);
    }
    if ($save) {
      try {
        $config->save();
      } catch (\Exception $e) {
        $this->logHelper->logError($e, $debug);
      }
    }
    return $config;
  }

  public function configLoad(string $configName): Config {
    return $this->configFactory->getEditable($configName);
  }

  /**
   * @param array|string|int|null $id
   *
   * @return string|int|null
   */
  public function extractId($id) {
    // This will produce nasty bugs due to php's string access:
    // $id = $values[$idKey][0]['value'] ?? $values[$idKey][0] ?? $values[$idKey]['value'] ?? $values[$idKey] ?? NULL;
    if (is_null($id) || is_string($id) || is_int($id)) {
      return $id;
    }
    if (is_array($id)) {
      $id = $id[0] ?? $id;
    }
    if (is_array($id)) {
      $id = $id['value'] ?? $id;
    }
    if (is_null($id) || is_string($id) || is_int($id)) {
      return $id;
    }
    return NULL;
  }

}
