<?php

namespace Drupal\webform_mapper\Utility;

use Drupal\Core\Render\Element;
use Drupal\webform\Entity\Webform;

class AutoMapper {

  public function automapin(string $webformId, array $data): array {
    $webform = Webform::load($webformId) ?? $this->throwInvalidWebformId($webformId);
    assert($webform instanceof Webform);
    $elementData = [];
    foreach ($this->extractMappings($webform) as $elementId => $dataPaths) {
      $value = WebformMapperNestedArray::get($data, $dataPaths[0]);
      if (isset($value)) {
        $targetElement =& WebformMapperNestedArray::get($elementData, $elementId);
        $targetElement = $value;
      }
    }
    return $elementData;
  }

  public function automapout(string $webformId, array $elementData): array {
    $webform = Webform::load($webformId) ?? $this->throwInvalidWebformId($webformId);
    assert($webform instanceof Webform);
    $data = [];
    foreach ($this->extractMappings($webform) as $elementId => $dataPaths) {
      foreach ($dataPaths as $dataPath) {
        $value = WebformMapperNestedArray::get($elementData, $elementId);
        if ($value) {
          $targetDataItem =& WebformMapperNestedArray::get($data, $dataPath);
          $targetDataItem = $value;
        }
      }
    }
    return $data;
  }

  protected function extractMappings(Webform $webform) {
    $this->doExtractMappings($webform->getElementsOriginalDecoded(), $mappings);
    return $mappings;
  }

  protected function doExtractMappings(array $elements, &$mappings = []): void {
    foreach ($elements as $elementId => $element) {
      $dataPaths = $element['#webform_mapper'] ?? NULL;
      if ($dataPaths) {
        $mappings[$elementId] = explode('|', $dataPaths);
      }
      foreach (Element::children($element) as $key) {
        $this->doExtractMappings($element[$key], $mappings);
      }
    }
  }

  private function throwInvalidWebformId(string $webformId) {
    throw new \UnexpectedValueException("Webform does not exist: $webformId");
  }

}
