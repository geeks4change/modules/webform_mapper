<?php

namespace Drupal\webform_mapper\Utility;

use Symfony\Component\Yaml\Dumper;
use Symfony\Component\Yaml\Yaml as SymfonyYaml;

/**
 * Copy of WebformYaml with tag and object support.
 */
class WebformMapperYaml {

  /**
   * {@inheritdoc}
   */
  public static function encode($data) {
    // Convert \r\n to \n so that multiline strings are properly formatted.
    // @see \Symfony\Component\Yaml\Dumper::dump
    if (is_array($data)) {
      static::normalize($data);
    }

    // If empty array then return an empty string instead of '{ }'.
    if (is_array($data) && empty($data)) {
      return '';
    }

    $dumper = new Dumper(2);
    $yaml = $dumper->dump($data, PHP_INT_MAX, 0, SymfonyYaml::DUMP_EXCEPTION_ON_INVALID_TYPE | SymfonyYaml::DUMP_MULTI_LINE_LITERAL_BLOCK | SymfonyYaml::DUMP_OBJECT );

    // Remove return after array delimiter.
    $yaml = preg_replace('#((?:\n|^)[ ]*-)\n[ ]+(\w|[\'"])#', '\1 \2', $yaml);

    return trim($yaml);
  }

  /**
   * Convert \r\n to \n inside data.
   *
   * @param array $data
   *   Data with all converted \r\n to \n.
   */
  protected static function normalize(array &$data) {
    foreach ($data as $key => &$value) {
      if (is_string($value)) {
        $data[$key] = preg_replace('/\r\n?/', "\n", $value);
      }
      elseif (is_array($value)) {
        static::normalize($value);
      }
    }
  }

}
