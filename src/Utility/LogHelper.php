<?php

namespace Drupal\webform_mapper\Utility;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Markup;
use Drupal\webform\Utility\WebformElementHelper;
use Symfony\Component\Yaml\Tag\TaggedValue;

class LogHelper {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  public function __construct(MessengerInterface $messenger, LoggerChannelFactoryInterface $loggerFactory) {
    $this->messenger = $messenger;
    $this->loggerFactory = $loggerFactory;
  }

  protected function getLogger(string $channel = 'webform'): LoggerChannelInterface {
    return $this->loggerFactory->get($channel);
  }

  public function dumpData(array $data, string $title): void {
    // @see \Drupal\webform\Plugin\WebformHandler\DebugWebformHandler::submitForm
    WebformElementHelper::convertRenderMarkupToStrings($data);
    self::convertObjectsToTags($data);
    $yaml = WebformMapperYaml::encode($data);
    $this->messenger->addWarning(Markup::create("<details><summary>$title</summary><pre>$yaml</pre></details>"));
  }

  public function logError(\Throwable $e, bool $debug): void {
    $markup = Markup::create(sprintf('<details><summary>%s</summary><pre>%s</pre></details>', $e->getMessage(), $e->getTraceAsString()));
    if ($debug) {
      $this->messenger->addError($markup);
    }
    else {
      $this->messenger
        ->addError($this->t('An error occurred. Please contact the site administrator.'));
      $this->getLogger('webform_mapper')->error($markup);
    }
  }

  private static function convertObjectsToTags(&$data) {
    if ($data instanceof EntityInterface) {
      $class = get_class($data);
      $data = new TaggedValue("nice-entity-dump/$class", $data->toArray());
    }
    elseif (is_array($data)) {
      foreach ($data as $key => &$value) {
        self::convertObjectsToTags($value);
      }
    }
  }

}
