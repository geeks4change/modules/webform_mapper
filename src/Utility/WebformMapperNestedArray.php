<?php

namespace Drupal\webform_mapper\Utility;

class WebformMapperNestedArray {

  public static function &get(&$data, string $path) {
    if (!$path) {
      return $data;
    }
    [$first, $rest] = explode('.', $path, 2) + [1 => ''];
    if (is_null($data) || is_array($data)) {
      return self::get($data[$first], $rest);
    }
    else {
      throw new \UnexpectedValueException(sprintf("Can not add key $first on scalar '%s'", var_export($data, TRUE)));
    }
  }

}
