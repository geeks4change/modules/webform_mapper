<?php

namespace Drupal\webform_mapper\Utility;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform_mapper\Navigator\ArrayNavigator;
use Drupal\webform_mapper\Navigator\ArrayReadOnlyNavigator;
use Drupal\webform_mapper\Navigator\NavigatorNavigator;
use Symfony\Component\HttpFoundation\RequestStack;

class NavigatorFactory {

  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * NavigatorFactory constructor.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   * @param \Drupal\Component\Datetime\TimeInterface $time
   */
  public function __construct(RequestStack $requestStack, AccountInterface $currentUser, TimeInterface $time) {
    $this->requestStack = $requestStack;
    $this->currentUser = $currentUser;
    $this->time = $time;
  }

  public function makeNavigator(string $trigger, WebformSubmissionInterface $webformSubmission, bool $dry_run, bool $debug): NavigatorNavigator {
    $webformData = $webformSubmission->getData();
    // Prepare source and target as readonly vars, target writable.
    $navigators = [];
    $navigators['trigger'] = ArrayReadOnlyNavigator::create($trigger);
    $navigators['timestamp'] = ArrayReadOnlyNavigator::create($this->time->getRequestTime());
    $navigators['ip'] = ArrayReadOnlyNavigator::create($this->requestStack->getCurrentRequest()
      ->getClientIp());
    $navigators['uid'] = ArrayReadOnlyNavigator::create($this->currentUser->id());
    $navigators['email'] = ArrayReadOnlyNavigator::create($this->currentUser->getEmail());
    $navigators['submit'] = ArrayReadOnlyNavigator::create($webformSubmission->isCompleted());
    $navigators['dry_run'] = ArrayReadOnlyNavigator::create($dry_run);
    $navigators['debug'] = ArrayReadOnlyNavigator::create($debug);
    $navigators['data'] = ArrayReadOnlyNavigator::create($webformData);
    // @todo Figure out how to do this really.
    if ($webformSubmission->getOriginalData()) {
      $navigators['data_original'] = ArrayReadOnlyNavigator::create($webformData);
    }
    // Prepopulate data only has effect on postcreate, so provide it only there.
    // We set the variable also for postload, so mappers don't throw.
    if ($trigger === 'postcreate' || $trigger === 'postload') {
      $prepopulateData = $trigger !== 'postcreate' ? []
        : WebformPrepopulate::create($webformSubmission)->getPrepopulateData();
      $navigators['data_prepopulate'] = ArrayReadOnlyNavigator::create($prepopulateData);
    }
    // On postsave, we can not do changes.
    if ($trigger !== 'postsave') {
      $navigators['data_changes'] = ArrayNavigator::create([]);
    }
    $navigators['webform_submission'] = ArrayReadOnlyNavigator::create($webformSubmission->toArray());
    // @todo Figure out how to do this really.
    if (isset($webformSubmission->original)) {
      $navigators['webform_submission_original'] = ArrayReadOnlyNavigator::create($webformSubmission->toArray());
    }
    $navigators['var'] = ArrayNavigator::create([]);
    $navigator = NavigatorNavigator::create($navigators);
    return $navigator;
  }

}
