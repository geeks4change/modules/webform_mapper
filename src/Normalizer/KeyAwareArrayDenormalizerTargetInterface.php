<?php

namespace Drupal\webform_mapper\Normalizer;

interface KeyAwareArrayDenormalizerTargetInterface {

  function setArrayDenormalizerKey($key);

}
