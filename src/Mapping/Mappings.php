<?php

namespace Drupal\webform_mapper\Mapping;

use Drupal\webform_mapper\Navigator\NavigatorNavigator;
use Symfony\Component\Serializer\Normalizer\DenormalizableInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizableInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class Mappings implements NormalizableInterface, DenormalizableInterface {

  /**
   * @var \Drupal\webform_mapper\Mapping\Mapping[]
   */
  public $mappings;

  /**
   * @inheritDoc
   */
  public function normalize(NormalizerInterface $normalizer, $format = NULL, array $context = []) {
    $data = [];
    foreach ($this->mappings as $mapping) {
      // @fixme Delegate normalizing.
      $data[$mapping->getTarget()->toString()] = $mapping->getExpression()->toString();
    }
    return $data;
  }

  /**
   * @inheritDoc
   */
  public function denormalize(DenormalizerInterface $denormalizer, $data, $format = NULL, array $context = []) {
    foreach ($data as $targetString => $expressionString) {
      $target = $denormalizer->denormalize($targetString, Target::class);
      assert($target instanceof Target);
      $expression = $denormalizer->denormalize($expressionString, Expression::class);
      assert($expression instanceof Expression);
      $this->mappings[] = new Mapping($target, $expression);
    }
  }

  public function map(NavigatorNavigator $navigator) {
    $expressionVariables = $navigator->getNavigators();

    foreach ($this->mappings as $mapping) {
      $value = $mapping->getExpression()->evaluate($expressionVariables);
      $targetPath = $mapping->getTarget()->toString();
      $navigator->setDataByPath($targetPath, $value);
    }
  }

}
