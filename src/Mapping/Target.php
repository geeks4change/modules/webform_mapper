<?php

namespace Drupal\webform_mapper\Mapping;

use Symfony\Component\Serializer\Normalizer\DenormalizableInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizableInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class Target implements NormalizableInterface, DenormalizableInterface {

  /**
   * @var string
   */
  protected $target;

  public function setTarget(string $target): void {
    $this->target = $target;
  }

  /**
   * @inheritDoc
   */
  public function denormalize(DenormalizerInterface $denormalizer, $data, $format = NULL, array $context = []) {
    // @todo Validate.
    $this->setTarget($data);
  }

  /**
   * @inheritDoc
   */
  public function normalize(NormalizerInterface $normalizer, $format = NULL, array $context = []) {
    return $this->target;
  }

  public function toString() {
    return $this->target;
  }

}
