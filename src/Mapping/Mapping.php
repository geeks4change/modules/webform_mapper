<?php

namespace Drupal\webform_mapper\Mapping;

use Symfony\Component\Serializer\Annotation\Groups;

class Mapping {

  /**
   * @var \Drupal\webform_mapper\Mapping\Target
   * @Groups("MappingGroup")
   */
  protected $target;

  /**
   * @var \Drupal\webform_mapper\Mapping\Expression
   * @Groups("MappingGroup")
   */
  protected $expression;

  /**
   * Mapping constructor.
   *
   * @param \Drupal\webform_mapper\Mapping\Target $target
   * @param \Drupal\webform_mapper\Mapping\Expression $expression
   */
  public function __construct(Target $target, Expression $expression) {
    $this->target = $target;
    $this->expression = $expression;
  }

  /**
   * @return \Drupal\webform_mapper\Mapping\Target
   */
  public function getTarget(): Target {
    return $this->target;
  }

  /**
   * @return \Drupal\webform_mapper\Mapping\Expression
   */
  public function getExpression(): Expression {
    return $this->expression;
  }

}
