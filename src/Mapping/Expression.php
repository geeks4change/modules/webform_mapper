<?php

namespace Drupal\webform_mapper\Mapping;

use Drupal\webform_mapper\Navigator\ArrayReadOnlyNavigator;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\Serializer\Normalizer\DenormalizableInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizableInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Yaml\Tag\TaggedValue;

class Expression implements NormalizableInterface, DenormalizableInterface {

  /**
   * @var string
   */
  protected $expression;

  /**
   * @var \Symfony\Component\ExpressionLanguage\ExpressionLanguage
   */
  protected $expressionLanguage;

  /**
   * @return \Symfony\Component\ExpressionLanguage\ExpressionLanguage
   */
  public function getExpressionLanguage(): ExpressionLanguage {
    if (!$this->expressionLanguage) {
      $this->expressionLanguage = \Drupal::service('webform_mapper.expression_language');
    }
    return $this->expressionLanguage;
  }

  public function setExpression($expression): void {
    self::validateExpression($expression);
    $this->expression = $expression;
  }

  public function denormalize(DenormalizerInterface $denormalizer, $data, $format = NULL, array $context = []) {
    $this->setExpression($data);
  }

  public function normalize(NormalizerInterface $normalizer, $format = NULL, array $context = []) {
    return $this->expression;
  }

  public function toString() {
    return $this->expression;
  }

  public function evaluate(array $variables) {
    return self::evaluateExpression($this->expression, $variables, $this->getExpressionLanguage());
  }

  protected static function validateExpression(&$expression): void {
    if ($expression instanceof TaggedValue) {
      if ($expression->getTag() === 'expression') {
        $value = $expression->getValue();
        if (is_string($value)) {
          // That's good.
        }
        // @todo Remove when we can rely on D9 / Symfony4.
        elseif (is_array($value) && (count($value) === 1) && is_string($value[0])) {
          $expression = new TaggedValue($expression->getTag(), $value[0]);
        }
        else {
          throw new \UnexpectedValueException("Expression must be string: " . var_export($value, TRUE));
        }
      }
      else {
        throw new \UnexpectedValueException("Unknown tag: {$expression->getTag()}");
      }
    }
    elseif (is_array($expression)) {
      foreach ($expression as &$item) {
        self::validateExpression($item);
      }
    }
    else {
      $type = gettype($expression);
      if (!in_array($type, ['boolean', 'integer', 'double', 'string', 'array', 'NULL'])) {
        throw new \UnexpectedValueException(sprintf("Constant expression must be scalar or array. Got %s (%s).", $type, var_export($expression, TRUE)));
      }
    }
  }

  protected static function evaluateExpression($expression, array $variables, ExpressionLanguage $expressionLanguage) {
    if ($expression instanceof TaggedValue) {
      if ($expression->getTag() === 'expression') {
        $value = $expression->getValue();
        if (is_string($value)) {
          $evaluated = $expressionLanguage->evaluate($value, $variables);
          return ArrayReadOnlyNavigator::unwrapIfNeeded($evaluated);
        }
        else {
          throw new \UnexpectedValueException("Expression must be string: " . var_export($value, TRUE));
        }
      }
      else {
        throw new \UnexpectedValueException("Unknown tag: {$expression->getTag()}");
      }
    }
    elseif (is_array($expression)) {
      foreach ($expression as &$item) {
        $item = self::evaluateExpression($item, $variables, $expressionLanguage);
      }
      return $expression;
    }
    else {
      return $expression;
    }
  }

}
