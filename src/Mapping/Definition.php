<?php

namespace Drupal\webform_mapper\Mapping;

use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\Serializer\Encoder\YamlEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\CustomNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Yaml\Yaml;

class Definition {

  /**
   * @var \Drupal\webform_mapper\Mapping\ConditionalMappings|null
   */
  public $mappings;

  /**
   * Definition constructor.
   *
   * @param \Drupal\webform_mapper\Mapping\ConditionalMappings|null $mappings
   */
  public function __construct(?ConditionalMappings $mappings) {
    $this->mappings = $mappings;
  }

  public static function fromYaml(string $yaml): Definition {
    $return = static::makeSerializer()->deserialize($yaml, self::class, 'yaml');
    assert($return instanceof Definition);
    return $return;
  }

  public function toYaml(): string {
    return static::makeSerializer()->serialize($this, 'yaml');
  }

  private static function makeSerializer() {
    $normalizers = [
      new CustomNormalizer(),
      new ObjectNormalizer(
        new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader())),
        new CamelCaseToSnakeCaseNameConverter(),
        NULL,
        new PropertyInfoExtractor()
      ),
    ];
    $serializer = new Serializer($normalizers, [
      new YamlEncoder(NULL, NULL, ['yaml_flags' => Yaml::PARSE_CUSTOM_TAGS])]
    );
    return $serializer;
  }

}
