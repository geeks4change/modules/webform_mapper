<?php

namespace Drupal\webform_mapper\Mapping;

use Drupal\webform_mapper\Navigator\NavigatorNavigator;
use Symfony\Component\Serializer\Normalizer\DenormalizableInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizableInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Yaml\Tag\TaggedValue;

class ConditionalMappings implements NormalizableInterface, DenormalizableInterface {

  /**
   * @var \SplObjectStorage
   *   keys: \Drupal\webform_mapper\Mapping\Expression
   *   values: \Drupal\webform_mapper\Mapping\Mappings
   */
  public $conditionalMappings;

  /**
   * ConditionalMappings constructor.
   */
  public function __construct() {
    $this->conditionalMappings = new \SplObjectStorage();
  }

  public function denormalize(DenormalizerInterface $denormalizer, $data, $format = NULL, array $context = []) {
    foreach ($data as $conditionString => $mappings) {
      $expression = (new Expression());
      $expression->setExpression(new TaggedValue('expression', $conditionString));
      $this->conditionalMappings[$expression] =
        $denormalizer->denormalize($mappings, Mappings::class);
    }
  }

  public function normalize(NormalizerInterface $normalizer, $format = NULL, array $context = []) {
    $data = [];
    foreach ($this->conditionalMappings as $expression) {
      $mappings = $this->conditionalMappings[$expression];
      $data[$normalizer->normalize($expression)] = $normalizer->normalize($mappings);
    }
    return $data;
  }

  public function map(NavigatorNavigator $navigator) {
    $expressionVariables = $navigator->getNavigators();

    foreach ($this->conditionalMappings as $expression) {
      $mappings = $this->conditionalMappings[$expression];
      assert($expression instanceof Expression);
      $condition = $expression->evaluate($expressionVariables);
      if ($condition) {
        assert($mappings instanceof Mappings);
        $mappings->map($navigator);
      }
    }
  }


}
